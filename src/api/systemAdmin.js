import request from '@/utils/request'

const api = {
  systemAdmin: '/admin/system/listAdmin'
}

export default api

export function getSystemAdminList (parameter) {
  return request({
    url: api.systemAdmin,
    method: 'POST',
    params: parameter
  })
}
