import request from '@/utils/request'

const api = {
  user: '/user',
  role: '/role',
  service: '/service',
  permission: '/permission',
  permissionNoPager: '/permission/no-pager',
  orgTree: '/org/tree',
  mockUserList: '/mockUserList',
  mockPartnerList: '/mockPartnerList',
  mockProductList: '/mockProductList',
  mockSellList: '/mockSellList',
  mockSubscribeList: '/mockSubscribeList',
  mockBookList: '/mockBookList',
  mockAdminList: '/mockAdminList',
  mockBannerList: '/mockBannerList',
  mockNoticeList: '/mockNoticeList',
  mockLogList: '/mockLogList'
}

export default api

export function getUserList (parameter) {
  return request({
    url: api.user,
    method: 'get',
    params: parameter
  })
}

export function getRoleList (parameter) {
  return request({
    url: api.role,
    method: 'get',
    params: parameter
  })
}

export function getServiceList (parameter) {
  return request({
    url: api.service,
    method: 'get',
    params: parameter
  })
}

export function getPermissions (parameter) {
  return request({
    url: api.permissionNoPager,
    method: 'get',
    params: parameter
  })
}

export function getOrgTree (parameter) {
  return request({
    url: api.orgTree,
    method: 'get',
    params: parameter
  })
}

// id == 0 add     post
// id != 0 update  put
export function saveService (parameter) {
  return request({
    url: api.service,
    method: parameter.id === 0 ? 'post' : 'put',
    data: parameter
  })
}

export function saveSub (sub) {
  return request({
    url: '/sub',
    method: sub.id === 0 ? 'post' : 'put',
    data: sub
  })
}

export function mockUserList (parameter) {
  const dataArr = []
  for (var i = 0; i < 10; i++) {
    dataArr.push({
      rowKey: i,
      key: i,
      id: i,
      no: i,
      username: '1501111111' + i,
      name: '张三' + i,
      address: '浙江杭州',
      belongPartner: '杭州市区负责人' + i,
      regAt: '2020-01-01 00:00:00',
      editable: false
    })
  }
  return new Promise(resolve => {
    resolve({ result: {
    data: dataArr,
    pageNo: 1,
    pageSize: 10,
    totalCount: 5701,
    totalPage: 571
  }
})
  })
}
